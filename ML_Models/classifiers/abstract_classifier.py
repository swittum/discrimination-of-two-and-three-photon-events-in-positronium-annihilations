from abc import ABC, abstractmethod


class AbstractClassifier(ABC):


    @abstractmethod
    def train(self, x, y):
        ...


    @abstractmethod
    def predict(self, x):
        ...


    @abstractmethod
    def get_roc(self, x, y):
        ...


    @abstractmethod
    def get_accuracy(self, x, y):
        ...


    @abstractmethod
    def get_cross_val_score(self, X, y, metric, n_splits, **kwargs):
        ...