#!./.venv/bin/python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../')

import numpy as np
import matplotlib.pyplot as plt
import xgboost as xgb
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import roc_curve, roc_auc_score
from generate_data import generate_data
from classifiers.abstract_classifier import AbstractClassifier


class BDT_Classifier(AbstractClassifier):
    

    def __init__(self, boosting_mode='xgb', n_estimators=100, max_depth=3, learning_rate=0.1, **kwargs):
        if boosting_mode == 'xgb':
            self._clf = xgb.XGBClassifier(n_estimators=n_estimators, max_depth=max_depth, learning_rate=learning_rate, use_label_encoder=False, eval_metric='logloss', **kwargs)
        if boosting_mode == 'ada':
            base_estimator = DecisionTreeClassifier(max_depth=max_depth)
            self._clf = AdaBoostClassifier(base_estimator=base_estimator, n_estimators=n_estimators, learning_rate=learning_rate, **kwargs)
        self._is_trained = False


    def train(self, x_train, y_train):
        self._clf.fit(x_train, y_train)
        self._is_trained = True


    def predict(self, x, threshold=0.5):
        probs = self._clf.predict_proba(x)[:, 1]
        return (probs > threshold).astype(int)
            

    def get_roc(self, X_test, y_test):
        y_predicted = self._clf.predict_proba(X_test)[:, 1]
        fpr, tpr, thresholds = roc_curve(y_test, y_predicted)
        return fpr, tpr, thresholds
    

    def get_auc(self, X_test, y_test):
        y_predicted = self._clf.predict_proba(X_test)[:, 1]
        return roc_auc_score(y_test, y_predicted)
    

    def get_accuracy(self, X_test, y_test):
        y_predicted = self.predict(X_test)
        return np.mean(y_predicted == y_test)
    

    def get_cross_val_score(self, X, y, metrics=['acc'], n_splits=5, **kwargs):
        kf = KFold(n_splits=n_splits, **kwargs)
        scores = []
        for train_index, test_index in kf.split(X):
            vals = []
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            self.train(X_train, y_train)
            if 'acc' in metrics:
                vals.append(self.get_accuracy(X_test, y_test))
            if 'auc' in metrics:
                _, _, _, auc = self.get_roc(X_test, y_test)
                vals.append(auc)
            if 'sig' in metrics:
                vals.append(self.get_significance(X_test, y_test, threshold=0.5))
            scores.append(vals)
        scores = np.array(scores)
        return scores
    

    def get_significance(self, X_test, y_test, threshold=0.5):
        y_predicted = self.predict(X_test, threshold)
        n_signal = np.sum(y_predicted[y_test==1])
        n_background = np.sum(y_predicted[y_test==0])
        significance = n_signal/np.sqrt(n_signal+n_background)
        return significance
    

    def optimize_threshold(self, X_test, y_test):
        thresholds = np.linspace(0, 1, 100)
        significances = np.array([self.get_significance(X_test, y_test, threshold) for threshold in thresholds])
        significances[np.isnan(significances)] = -np.inf
        best_threshold = thresholds[np.argmax(significances)]
        return best_threshold
    

    def get_best_significance(self, X_test, y_test):
        threshold = self.optimize_threshold(X_test, y_test)
        return self.get_significance(X_test, y_test, threshold)
        

def main():
    # Generate signal data
    signal_mu = [0, 0]
    signal_sigma = [1, 1]
    signal_n_points = 100
    signal_x, signal_y, signal_value = generate_data(signal_mu, signal_sigma, True, signal_n_points)

    # Generate background data
    background_mu = [0.9, 1.2]
    background_sigma = [1, 1.3]
    background_n_points = 100
    background_x, background_y, background_value = generate_data(background_mu, background_sigma, False, background_n_points)

    # Concatenate signal and background data
    data_x = np.concatenate((signal_x, background_x))
    data_y = np.concatenate((signal_y, background_y))
    data_value = np.concatenate((signal_value, background_value))

    # Split data into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        np.column_stack((data_x, data_y)),
        data_value,
        test_size=0.2,
        random_state=42
    )

    # Evaluate the classifier
    classifier = BDT_Classifier()
    classifier.train(X_train, y_train)
    accuracy = classifier.get_accuracy(X_test, y_test)
    print(f'Accuracy: {accuracy:.4f}')

    # Get ROC and AUC
    fpr, tpr, _, auc = classifier.get_roc(X_test, y_test)
    print(f'AUC: {auc:.4f}')

    # Plotting the ROC curve
    plt.plot(fpr, tpr)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('AUC: {:.4f}'.format(auc))
    plt.legend(['Self', 'Sklearn'])
    plt.grid()
    plt.show()


if __name__ == '__main__':
    # main()
    model = BDT_Classifier(boosting_mode='ada', n_estimators=20, max_depth=1, learning_rate=1)

    signal_mu = [0, 0]
    signal_sigma = [1, 1]
    signal_n_points = 100
    signal_x, signal_y, signal_value = generate_data(signal_mu, signal_sigma, True, signal_n_points)

    # Generate background data
    background_mu = [0.9, 1.2]
    background_sigma = [1, 1.3]
    background_n_points = 100
    background_x, background_y, background_value = generate_data(background_mu, background_sigma, False, background_n_points)

    # Concatenate signal and background data
    data_x = np.concatenate((signal_x, background_x))
    data_y = np.concatenate((signal_y, background_y))
    X = np.column_stack((data_x, data_y))
    y = np.concatenate((signal_value, background_value))
    data = np.column_stack((X, y))
    np.random.shuffle(data)
    X, y = data[:, :-1], data[:, -1]

    metrics = ['acc', 'auc']
    result = model.get_cross_val_score(X, y, metrics=metrics, n_splits=5)
    means = np.mean(result, axis=0)
    stds = np.std(result, axis=0)
    print(means)
    print(stds)

       
