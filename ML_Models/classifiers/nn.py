
#!./.venv/bin/python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
from torch.nn import functional as F
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import roc_curve, roc_auc_score
from generate_data import generate_data
from classifiers.abstract_classifier import AbstractClassifier


class NN_Classifier(nn.Module, AbstractClassifier):


    def __init__(self, architecture=[2, 64, 1]):
        super(NN_Classifier, self).__init__()
        self.layers = nn.ModuleList()
        self.n_layers = len(architecture)-1
        for i in range(self.n_layers):
            self.layers.append(nn.Linear(architecture[i], architecture[i+1]))
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)
        self.sigmoid = nn.Sigmoid()
        self._is_trained = False


    def forward(self, X):
        if isinstance(X, np.ndarray):
            X = torch.tensor(X, dtype=torch.float32)
        for i in range(self.n_layers-1):
            X = torch.relu(self.layers[i](X))
        y = self.sigmoid(self.layers[-1](X))
        return y
    
    
    def train(self, X_train, y_train, lr=0.01, num_epochs=100, *, verbose=False):
        criterion = nn.BCELoss()
        optimizer = optim.SGD(self.parameters(), lr=lr)
        if isinstance(X_train, np.ndarray):
            X_train = torch.tensor(X_train, dtype=torch.float32)
        if isinstance(y_train, np.ndarray):
            y_train = torch.tensor(y_train, dtype=torch.float32)
        train_data = TensorDataset(X_train, y_train)
        train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
        for epoch in range(num_epochs):
            for inputs, labels in train_loader:
                optimizer.zero_grad()
                outputs = self(inputs)
                loss = criterion(outputs, labels.unsqueeze(1))
                loss.backward()
                optimizer.step()
            if verbose and (epoch + 1) % 10 == 0:
                print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')
        self._is_trained = True


    def predict(self, X, threshold=0.5):
        out = self.forward(X)
        return np.array([1 if value > threshold else 0 for value in out.detach().numpy()])
    

    def get_roc(self, X_test, y_test):
        if not self._is_trained:
            self.train(X_test, y_test, verbose=False)
        y_predicted = self.forward(X_test).squeeze().detach().numpy()
        fpr, tpr, thresholds = roc_curve(y_test, y_predicted)
        return fpr, tpr, thresholds
    

    def get_auc(self, X_test, y_test):
        y_predicted = self.forward(X_test).squeeze().detach().numpy()
        auc = roc_auc_score(y_test, y_predicted)
        return auc
    

    def get_accuracy(self, X_test, y_test, threshold=0.5):
        y_predicted = self.predict(X_test, threshold)
        if isinstance(y_test, torch.Tensor):
            y_test = y_test.numpy()
        accuracy = (y_predicted == y_test).mean()
        return accuracy
    

    def get_cross_val_score(self, X, y, metrics=['acc'], n_splits=5, **kwargs):
        kf = KFold(n_splits=n_splits, **kwargs)
        scores = []
        for train_index, test_index in kf.split(X):
            vals = []
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            self.train(X_train, y_train)
            if 'acc' in metrics:
                vals.append(self.get_accuracy(X_test, y_test))
            if 'auc' in metrics:
                _, _, _, auc = self.get_roc(X_test, y_test)
                vals.append(auc)
            if 'sig' in metrics:
                vals.append(self.get_significance(X_test, y_test, threshold=0.5))
            scores.append(vals)
        scores = np.array(scores)
        return scores
    

    def get_significance(self, X_test, y_test, threshold=0.5):
        y_predicted = self.predict(X_test, threshold)
        n_signal = np.sum(y_predicted[y_test==1])
        n_background = np.sum(y_predicted[y_test==0])
        significance = n_signal/np.sqrt(n_signal+n_background)
        return significance
    

    def optimize_threshold(self, X_test, y_test):
        thresholds = np.linspace(0, 1, 100)
        significances = np.array([self.get_significance(X_test, y_test, threshold) for threshold in thresholds])
        significances[np.isnan(significances)] = -np.inf
        best_threshold = thresholds[np.argmax(significances)]
        return best_threshold
    

    def get_best_significance(self, X_test, y_test):
        threshold = self.optimize_threshold(X_test, y_test)
        return self.get_significance(X_test, y_test, threshold)


def main():
    # Generate signal data
    signal_mu = [0, 0]
    signal_sigma = [1, 1]
    signal_n_points = 100
    signal_x, signal_y, signal_value = generate_data(signal_mu, signal_sigma, True, signal_n_points)

    # Generate background data
    background_mu = [0.9, 1.4]
    background_sigma = [1, 0.3]
    background_n_points = 100
    background_x, background_y, background_value = generate_data(background_mu, background_sigma, False, background_n_points)

    # Concatenate signal and background data
    data_x = np.concatenate((signal_x, background_x))
    data_y = np.concatenate((signal_y, background_y))
    data_value = np.concatenate((signal_value, background_value))

    X_train, X_test, y_train, y_test = train_test_split(
        np.column_stack((data_x, data_y)),
        data_value,
        test_size=0.2,
        random_state=42
    )   

    # Evaluate the classifier
    classifier = NN_Classifier()
    classifier.train(X_train, y_train, verbose=False)
    accuracy = classifier.get_accuracy(X_test, y_test)
    print(f'Accuracy: {accuracy:.4f}')

    # Get ROC and AUC
    fpr, tpr, _, auc = classifier.get_roc(X_test, y_test)
    print(f'AUC: {auc:.4f}')

    # Plotting the ROC curve
    plt.plot(fpr, tpr)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('AUC: {:.4f}'.format(auc))
    plt.legend(['Self', 'Sklearn'])
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()