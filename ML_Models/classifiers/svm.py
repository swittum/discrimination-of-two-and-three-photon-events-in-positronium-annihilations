#!./.venv/bin/python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import roc_curve, roc_auc_score
from generate_data import generate_data
from classifiers.abstract_classifier import AbstractClassifier


class SVM_Classifier(AbstractClassifier):


    def __init__(self, kernel='linear'):
        self._is_trained = False
        self._classifier = SVC(kernel=kernel)

    
    def train(self, X_train, y_train):
        self._classifier.fit(X_train, y_train)
        self._is_trained = True


    def predict(self, X_test):
        return self._classifier.predict(X_test)
    

    def get_roc(self, X_test, y_test):
        y_predict = self._classifier.decision_function(X_test)
        fpr, tpr, thresholds = roc_curve(y_test, y_predict)
        auc = roc_auc_score(y_test, y_predict)
        return fpr, tpr, thresholds, auc
    
    
    def get_accuracy(self, X_test, y_test):
        y_predict = self.predict(X_test)
        return np.mean(y_predict == y_test)
    

    def get_cross_val_score(self, X, y, metrics=['acc'], n_splits=5, **kwargs):
        kf = KFold(n_splits=n_splits, **kwargs)
        scores = []
        for train_index, test_index in kf.split(X):
            vals = []
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            self.train(X_train, y_train)
            if 'acc' in metrics:
                vals.append(self.get_accuracy(X_test, y_test))
            if 'auc' in metrics:
                _, _, _, auc = self.get_roc(X_test, y_test)
                vals.append(auc)
            scores.append(vals)
        scores = np.array(scores)
        return scores
    

def main():
    # Generate signal data
    signal_mu = [1.0, 1.0]
    signal_sigma = [0.5, 0.5]
    signal_n_points = 1000
    signal_x, signal_y, signal_value = generate_data(signal_mu, signal_sigma, True, signal_n_points)

    # Generate background data
    background_mu = [-1.0, -1.0]
    background_sigma = [1.0, 1.0]
    background_n_points = 1000
    background_x, background_y, background_value = generate_data(background_mu, background_sigma, False, background_n_points)

    # Concatenate signal and background data
    data_x = np.concatenate((signal_x, background_x))
    data_y = np.concatenate((signal_y, background_y))
    data_value = np.concatenate((signal_value, background_value))

    # Split data into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        np.column_stack((data_x, data_y)),
        data_value,
        test_size=0.2,
        random_state=42
    )

    svm = SVM_Classifier()
    svm.train(X_train, y_train)
    y_predict = svm.predict(X_test)
    accuracy = np.mean(y_predict == y_test)
    print(f'Accuracy: {accuracy:.4f}')

    fpr, tpr, _, auc = svm.get_roc(X_test, y_test)
    print(f'AUC: {auc:.4f}')

    plt.scatter(fpr, tpr)
    plt.show()


if __name__ == "__main__":
    main()
    