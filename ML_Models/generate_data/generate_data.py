#!./.venv/bin/python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt


def generate_data(mu, sigma, label, n_points, random_state=None):
    """
    Generate data for the classifier
    :param mu: mean
    :param sigma: standard deviation
    :param label: label for the data
    :param n_points: number of points
    :param random_state: random state
    :return: x, y, value
    """
    np.random.seed(random_state)
    out = []
    for i in range(len(mu)):
        out.append(np.random.normal(mu[i], sigma[i], n_points))
    labels = np.array([label]*n_points)
    out = np.array(out)
    return *out, labels


def main():
    n_points = 100
    mu = [0, 0]
    sigma = [1, 1]
    x1, y1, _ = generate_data(mu, sigma, 0, n_points)
    mu = [2, 2]
    sigma = [1, 0.3]
    x2, y2, _ = generate_data(mu, sigma, 1, n_points)
    plt.scatter(x1, y1, label='0')
    plt.scatter(x2, y2, label='1')
    plt.legend(loc='best')
    plt.grid()
    plt.show()


if __name__ == "__main__":
    main()