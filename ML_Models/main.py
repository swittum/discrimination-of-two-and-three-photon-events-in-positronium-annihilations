#!./.venv/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

# Import function for data generation
from generate_data import generate_data

# Import classifier classes
from classifiers import BDT_Classifier, NN_Classifier, SVM_Classifier


def main():
    # Generate signal data
    signal_mu = [1.0, 1.0]
    signal_sigma = [0.5, 0.5]
    signal_n_points = 1000
    signal_x, signal_y, signal_value = generate_data(signal_mu, signal_sigma, True, signal_n_points)

    # Generate background data
    background_mu = [-0.5, -0.5]
    background_sigma = [1.0, 2.0]
    background_n_points = 1000
    background_x, background_y, background_value = generate_data(background_mu, background_sigma, False, background_n_points)

    # Concatenate signal and background data
    data_x = np.concatenate((signal_x, background_x))
    data_y = np.concatenate((signal_y, background_y))
    data_value = np.concatenate((signal_value, background_value))

    # Split data into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        np.column_stack((data_x, data_y)),
        data_value,
        test_size=0.2,
        random_state=42
    )

    # Evaluate BDT
    bdt = BDT_Classifier()
    fpr_bdt, tpr_bdt, _, auc_bdt = bdt.get_roc(X_test, y_test)
    bdt_accuracy = bdt.get_accuracy(X_test, y_test)

    # Evaluate NN
    nn = NN_Classifier()
    fpt_nn, tpr_nn, _, auc_nn = nn.get_roc(X_test, y_test)
    nn_accuracy = nn.get_accuracy(X_test, y_test)

    # Evaluate SVM
    svm = SVM_Classifier()
    svm.train(X_train, y_train)
    fpr_svm, tpr_svm, _, auc_svm = svm.get_roc(X_test, y_test)
    svm_accuracy = svm.get_accuracy(X_test, y_test)

    # Create Plot
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))

    ax[0].scatter(signal_x, signal_y, label='Signal')
    ax[0].scatter(background_x, background_y, label='Background')
    ax[0].legend(loc='best')
    ax[0].grid()
    ax[0].set_xlabel('x')
    ax[0].set_ylabel('y')
    ax[0].set_title('Visualisation of the Data')

    ax[1].plot(fpr_bdt, tpr_bdt, label=f'BDT (AUC={auc_bdt:.3f}, ACC={bdt_accuracy:.3f})')
    ax[1].plot(fpt_nn, tpr_nn, label=f'NN (AUC={auc_nn:.3f}, ACC={nn_accuracy:.3f})')
    ax[1].plot(fpr_svm, tpr_svm, label=f'SVM (AUC={auc_svm:.3f}, ACC={svm_accuracy:.3f})')
    ax[1].legend(loc='best')
    ax[1].grid()
    ax[1].set_xlabel('False Positive Rate')
    ax[1].set_ylabel('True Positive Rate')
    ax[1].set_title('ROC Curves')   

    # plt.savefig('./plots/compare_classifiers.png', dpi=300)
    plt.show()


if __name__ == "__main__":
    main()

