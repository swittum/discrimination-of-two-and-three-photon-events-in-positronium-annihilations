import numpy as np
import h5py


# Macros
N_MODULES = 2
N_CRYSTALS = 128


class MeasurementAnalyzer:


    def __init__(self, filename):
        f1 = h5py.File(filename, 'r')
        self.tge_raw = f1['trigger_gated_events']
        self.n_events_raw = len(self.tge_raw)

        # Initialize attributes to be defined later
        self.tge = self.tge_raw
        self.filtered_groups = None
        self.cluster_data = None
        self._n_filtered_groups = None


    @property
    def n_filtered_groups(self):
        if self.filtered_groups is None: raise ValueError("No clustering applied yet")
        return len(self.filtered_groups)


    def apply_crystal_cut(self, energy_window=[100, 1_000]):
        E_min, E_max = energy_window
        tge = [event for event in self.tge_raw if E_min < event['en'] < E_max]
        self.tge = np.array(tge)


    def apply_clustering(self, delta_t_threshold=2_000, verbose=False):
        # Necessary condition: Trigger time
        time_groups = {}
        for i, event in enumerate(self.tge_raw):
            if verbose and i % 100_000 == 0: print(i/self.n_events_raw)
            time = event['trig_time']
            if time not in time_groups: time_groups[time] = []
            # Weird bug: Events appear multiple times in dataset
            if event not in time_groups[time]: time_groups[time].append(event)
            # time_groups[time].append(event)

        # Sufficient condition: Time difference
        self.filtered_groups = []
        for i, (time, groups) in enumerate(time_groups.items()):
            # if i % 100_000 == 0: print(i/len(time_groups))
            sorted_group = sorted(groups, key=lambda x: x['delta_t'])
            current_group = [sorted_group[0]]
            for i in range(1, len(sorted_group)):
                if np.abs(sorted_group[i]['delta_t']-current_group[-1]['delta_t']) < delta_t_threshold:
                    current_group.append(sorted_group[i])
                else:
                    self.filtered_groups.append(current_group)
                    current_group = [sorted_group[i]]
            self.filtered_groups.append(current_group)


    def apply_module_cut(self, energy_window=[100, 1_000], coincidence=True):
        if self.filtered_groups is None: raise ValueError("No clustering applied yet")
        E_min, E_max = energy_window
        filtered_groups = []
        for group in self.filtered_groups:
            energy_A = 0
            energy_B = 0
            for measurement in group:
                if measurement['id'] < 128:
                    energy_A += measurement['en']
                if measurement['id'] >= 256:
                    energy_B += measurement['en']
            if coincidence and E_min < energy_A < E_max and E_min < energy_B < E_max:
                filtered_groups.append(group)
            elif not coincidence and (E_min < energy_A < E_max or E_min < energy_B < E_max):
                filtered_groups.append(group)
        self.filtered_groups = filtered_groups

                
    def full_process(self, crystal_window, energy_window, delta_t_threshold, coincidence=True, verbose=False):
        self.apply_crystal_cut(crystal_window)
        self.apply_clustering(delta_t_threshold, verbose=verbose)
        self.apply_module_cut(energy_window, coincidence=True)


    def get_cluster_data(self):
        # Average over delta_t and integrate over energy
        if self.filtered_groups is None: raise ValueError("No clustering applied yet")
        cluster_data = np.zeros((self.n_filtered_groups, 4))
        for i, group in enumerate(self.filtered_groups):
            time = np.mean([measurement['delta_t'] for measurement in group])
            energy_A = 0
            energy_B = 0
            for measurement in group:
                if measurement['id'] < 128:
                    energy_A += measurement['en']
                if measurement['id'] >= 256:
                    energy_B += measurement['en']
            cluster_data[i] = np.array([time, energy_A, energy_B, len(group)])
        return cluster_data
    

def main():
    print('Entering main')
    filename = "/Users/simonwittum/Documents/Uni/Master/Semester2/SemesterProject/Data_Valentin/24_05_08_data/run_1_group_100ns_r20_triger_gated_group_delay-158000_gatewindow1000000.hdf5"
    An = Analyzer(filename)
    An.apply_crystal_cut(energy_window=[100, 1_000])
    An.apply_clustering(delta_t_threshold=2_000, verbose=True)
    An.apply_module_cut(energy_window=[100, 1_000], coincidence=True)
    cluster_data = An.get_cluster_data()
    print(cluster_data.shape)
    print('Exiting main')


if __name__ == "__main__":
    main()

        

        

        

        