import numpy as np


# Detector parameters
N_MODULES = 2
N_LYSOUNITS = 2
N_LYSOBLOCKS = 1
N_LYSOPERBLOCKX = 8
N_LYSOPERBLOCKY = 8
N_CRYSTALS = N_LYSOUNITS*N_LYSOBLOCKS*N_LYSOPERBLOCKX*N_LYSOPERBLOCKY


# Utility functions
def get_x_index(i_crystal):
    return i_crystal%N_LYSOPERBLOCKX


def get_unit_index(i_crystal):
    i_x = get_x_index(i_crystal)
    return ((i_crystal-i_x)%(N_LYSOPERBLOCKX*N_LYSOUNITS))//N_LYSOPERBLOCKX


def get_y_index(i_crystal):
    i_x = get_x_index(i_crystal)
    i_unit = get_unit_index(i_crystal)
    return ((i_crystal-i_x-i_unit*N_LYSOPERBLOCKX)%N_LYSOPERBLOCKY)//(N_LYSOUNITS*N_LYSOBLOCKS)


def balance_dataset(X, y):
    unique_classes, class_counts = np.unique(y, return_counts=True)
    min_class_count = np.min(class_counts)

    balanced_X = []
    balanced_y = []
    for class_label in unique_classes:
        indices = np.where(y == class_label)[0]
        selected_indices = np.random.choice(indices, min_class_count, replace=False)
        balanced_X.extend(X[selected_indices])
        balanced_y.extend(y[selected_indices])
    balanced_X = np.array(balanced_X)
    balanced_y = np.array(balanced_y)

    shuffle_indices = np.random.permutation(len(balanced_X))
    balanced_X = balanced_X[shuffle_indices]
    balanced_y = balanced_y[shuffle_indices]
    return balanced_X, balanced_y


class SimulationAnalyzer:
    

    def __init__(self, file_pPs, file_oPs):
        """
        Constructor of the Analyzer class. The class is used to analyze the data of Geant4 simulation
        of p-Ps and o-Ps annihilation events.
        - param file_pPs: str, file path of the data of p-Ps annihilation events (.txt after preprocessing)
        - param file_oPs: str, file path of the data of o-Ps annihilation events (.txt after preprocessing)
        """
        # Organize pPs data
        self.energy_pPs = np.loadtxt(file_pPs)
        self._n_pPs = len(self.energy_pPs)
        
        # Organize oPs data
        self.energy_oPs = np.loadtxt(file_oPs)
        self._n_oPs = len(self.energy_oPs)

        # Check if data is preprocessed
        self.integration_mode = None
        self._n_samples = self.n_pPs+self.n_oPs

        # Placeholder for preprocessed data
        self.energy_pPs_p = None
        self.energy_oPs_p = None

        # Backup data
        self.energy_pPs_raw = self.energy_pPs
        self.energy_oPs_raw = self.energy_oPs
        self.n_pPs_raw = self.n_pPs
        self.n_oPs_raw = self.n_oPs


    @property
    def n_pPs(self):
        self._n_pPs = len(self.energy_pPs)
        return self._n_pPs
    

    @property
    def n_oPs(self):
        self._n_oPs = len(self.energy_oPs)
        return self._n_oPs
    

    @property
    def n_samples(self):
        return self.n_pPs+self.n_oPs 
    

    def reset(self):
        self.energy_pPs = self.energy_pPs_raw
        self.energy_oPs = self.energy_oPs_raw


    def get_module_energy(self, decay_mode):
        """
        Safely access latest data of pPs and oPs events splitted into the two modules
        - param decay_mode: str, 'pPs' or 'oPs'
        - return: tuple, (energy_A, energy_B) where energy_A and energy_B are the energies of the two modules
        """
        if decay_mode == 'pPs':
            return self.energy_pPs[:, :N_CRYSTALS], self.energy_pPs[:, N_CRYSTALS:]
        if decay_mode == 'oPs':
            return self.energy_oPs[:, :N_CRYSTALS], self.energy_oPs[:, N_CRYSTALS:]
        

    def apply_smearing(self, smearing, reference_energy=662, type='FWHM'):
        """
        Apply energy smearing to the data of pPs and oPs events and update the data
        - param smearing: float, standard deviation of the Gaussian smearing
        - param reference_energy: float, energy for the which the smearing is valid
        - param type: str, 'FWHM' or 'sigma' determines the type of the smearing
        - return: None
        """
        conversion_factor = 1/(2*np.sqrt(2*np.log(2))) if type == 'FWHM' else 1
        sigma_energy_pPs = conversion_factor*smearing*reference_energy*np.sqrt(self.energy_pPs/reference_energy)
        sigma_energy_oPs = conversion_factor*smearing*reference_energy*np.sqrt(self.energy_oPs/reference_energy)
        self.energy_pPs = np.random.normal(self.energy_pPs, sigma_energy_pPs)
        self.energy_oPs = np.random.normal(self.energy_oPs, sigma_energy_oPs)
        # self.energy_pPs = np.random.normal(self.energy_pPs, smearing*self.energy_pPs)
        # self.energy_oPs = np.random.normal(self.energy_oPs, smearing*self.energy_oPs)


    def apply_module_cut(self, energy_window, coincidence=False):
        """
        Apply energy filter to the data of pPs and oPs events and update the data
        - param energy_window: tuple, (E_min, E_max) energy window for the filter
        - param coincidence: bool, apply coincidence filter to the data
        - return: None
        """
        # Apply coincidence filter to pPs data
        E_min, E_max = energy_window
        energy_pPs_A = []
        energy_pPs_B = []
        for (e1, e2) in zip(*self.get_module_energy(decay_mode='pPs')):
            if coincidence and E_min <= np.sum(e1) <= E_max and E_min <= np.sum(e2) <= E_max:
                energy_pPs_A.append(e1)
                energy_pPs_B.append(e2)
            if not coincidence and (E_min <= np.sum(e1) <= E_max or E_min <= np.sum(e2) <= E_max):
                energy_pPs_A.append(e1)
                energy_pPs_B.append(e2)
        energy_pPs_A = np.array(energy_pPs_A)
        energy_pPs_B = np.array(energy_pPs_B)
        energy_pPs = np.column_stack((energy_pPs_A, energy_pPs_B))
        self.energy_pPs = energy_pPs
        # Apply coincidence_filter to oPs data
        energy_oPs_A = []
        energy_oPs_B = []
        for (e1, e2) in zip(*self.get_module_energy(decay_mode='oPs')):
            if coincidence and E_min <= np.sum(e1) <= E_max and E_min <= np.sum(e2) <= E_max:
                energy_oPs_A.append(e1)
                energy_oPs_B.append(e2)
            if not coincidence and (E_min <= np.sum(e1) <= E_max or E_min <= np.sum(e2) <= E_max):
                energy_oPs_A.append(e1)
                energy_oPs_B.append(e2)
        energy_oPs_A = np.array(energy_oPs_A)
        energy_oPs_B = np.array(energy_oPs_B)
        energy_oPs = np.column_stack((energy_oPs_A, energy_oPs_B))
        self.energy_oPs = energy_oPs


    def apply_crystal_cut(self, energy_window):
        """
        Apply energy filter to individual crystals within the LYSO modules
        - param energy_window: tuple, (E_min, E_max) energy window for the filter
        - return: None
        """
        E_min, E_max = energy_window
        # pPs data
        energy_pPs = []
        for event in self.energy_pPs:
            event_pPs = np.zeros(event.size)
            energies_A = event[:N_CRYSTALS]
            energies_B = event[N_CRYSTALS:]
            for i, e_A in enumerate(energies_A):
                if E_min <= e_A <= E_max:
                    event_pPs[i] = e_A
            for i, e_B in enumerate(energies_B):
                if E_min <= e_B <= E_max:
                    event_pPs[N_CRYSTALS+i] = e_B
            energy_pPs.append(event_pPs)
        self.energy_pPs = np.array(energy_pPs)
        # oPs data
        energy_oPs = []
        for event in self.energy_oPs:
            event_oPs = np.zeros(event.size)
            energies_A = event[:N_CRYSTALS]
            energies_B = event[N_CRYSTALS:]
            for i, e_A in enumerate(energies_A):
                if E_min <= e_A <= E_max:
                    event_oPs[i] = e_A                   
            for i, e_B in enumerate(energies_B):
                if E_min <= e_B <= E_max:
                    event_oPs[N_CRYSTALS+i] = e_B
            energy_oPs.append(event_oPs)
        self.energy_oPs = np.array(energy_oPs)
        

    def apply_preprocessing(self, integration_mode):
        """
        Apply preprocessing to the data of pPs and oPs event by integrating the energy of the crystals
        - param integration_mode: str, 'module', 'unit', 'y', 'x' or None
        - return: None
        """
        if integration_mode == 'module':
            # pPs data
            energy_pPs_A, energy_pPs_B = self.get_module_energy(decay_mode='pPs')
            self.energy_pPs_p = np.column_stack((np.sum(energy_pPs_A, axis=1), np.sum(energy_pPs_B, axis=1)))
            # oPs data
            energy_oPs_A, energy_oPs_B = self.get_module_energy(decay_mode='oPs')
            self.energy_oPs_p = np.column_stack((np.sum(energy_oPs_A, axis=1), np.sum(energy_oPs_B, axis=1)))

        if integration_mode == 'unit':
            # pPs data
            self.energy_pPs_p = np.zeros((self.n_pPs, N_MODULES*N_LYSOUNITS))
            energy_pPs_A, energy_pPs_B = self.get_module_energy(decay_mode='pPs')
            for i_sample, (event1, event2) in enumerate(zip(energy_pPs_A, energy_pPs_B)):
                for i_crystal, (energy1, energy2) in enumerate(zip(event1, event2)):
                    i_unit = get_unit_index(i_crystal)
                    self.energy_pPs_p[i_sample, i_unit] += energy1
                    self.energy_pPs_p[i_sample, i_unit+N_LYSOUNITS] += energy2
            # oPs data
            self.energy_oPs_p = np.zeros((self.n_oPs, N_MODULES*N_LYSOUNITS))
            energy_oPs_A, energy_oPs_B = self.get_module_energy(decay_mode='oPs')
            for i_sample, (event_A, event_B) in enumerate(zip(energy_oPs_A, energy_oPs_B)):
                for i_crystal, (energy_A, energy_B) in enumerate(zip(event_A, event_B)):
                    i_x = i_crystal%N_LYSOPERBLOCKX
                    i_unit = ((i_crystal-i_x)%(N_LYSOPERBLOCKX*N_LYSOUNITS))//N_LYSOPERBLOCKX
                    self.energy_oPs_p[i_sample, i_unit] += energy_A
                    self.energy_oPs_p[i_sample, i_unit+N_LYSOUNITS] += energy_B

        if integration_mode == 'y':
            # pPs data
            self.energy_pPs_p = np.zeros((self.n_pPs, N_MODULES*N_LYSOUNITS*N_LYSOBLOCKS*N_LYSOPERBLOCKX))
            energy_pPs_A, energy_pPs_B = self.get_module_energy(decay_mode='pPs')
            for i_sample, (event_A, event_B) in enumerate(zip(energy_pPs_A, energy_pPs_B)):
                for i_crystal, (energy_A, energy_B) in enumerate(zip(event_A, event_B)):
                    i_y = get_y_index(i_crystal)
                    i_unit = get_unit_index(i_crystal)
                    self.energy_pPs_p[i_sample, i_unit*N_LYSOPERBLOCKY+i_y] += energy_A
                    self.energy_pPs_p[i_sample, N_MODULES*N_LYSOUNITS+i_unit*N_LYSOPERBLOCKY+i_y] += energy_B
            # oPs data
            self.energy_oPs_p = np.zeros((self.n_oPs, N_MODULES*N_LYSOUNITS*N_LYSOBLOCKS*N_LYSOPERBLOCKX))
            energy_oPs_A, energy_oPs_B = self.get_module_energy(decay_mode='oPs')
            for i_sample, (event_A, event_B) in enumerate(zip(energy_oPs_A, energy_oPs_B)):
                for i_crystal, (energy_A, energy_B) in enumerate(zip(event_A, event_B)):
                    i_y = get_y_index(i_crystal)
                    i_unit = get_unit_index(i_crystal)
                    self.energy_oPs_p[i_sample, i_unit*N_LYSOPERBLOCKY+i_y] += energy_A
                    self.energy_oPs_p[i_sample, N_MODULES*N_LYSOUNITS+i_unit*N_LYSOPERBLOCKY+i_y] += energy_B

        if integration_mode == 'x':
            # pPs data
            self.energy_pPs_p = np.zeros((self.n_pPs, N_MODULES*N_LYSOUNITS*N_LYSOBLOCKS*N_LYSOPERBLOCKY))
            energy_pPs_A, energy_pPs_B = self.get_module_energy(decay_mode='pPs')
            for i_sample, (event_A, event_B) in enumerate(zip(energy_pPs_A, energy_pPs_B)):
                for i_crystal, (energy_A, energy_B) in enumerate(zip(event_A, event_B)):
                    i_x = get_x_index(i_crystal)
                    i_unit = get_unit_index(i_crystal)
                    self.energy_pPs_p[i_sample, i_unit*N_LYSOPERBLOCKX+i_x] += energy_A
                    self.energy_pPs_p[i_sample, N_MODULES*N_LYSOUNITS+i_unit*N_LYSOPERBLOCKX+i_x] += energy_B
            # oPs data
            self.energy_oPs_p = np.zeros((self.n_oPs, N_MODULES*N_LYSOUNITS*N_LYSOBLOCKS*N_LYSOPERBLOCKY))
            energy_oPs_A, energy_oPs_B = self.get_module_energy(decay_mode='oPs')
            for i_sample, (event_A, event_B) in enumerate(zip(energy_oPs_A, energy_oPs_B)):
                for i_crystal, (energy_A, energy_B) in enumerate(zip(event_A, event_B)):
                    i_x = get_x_index(i_crystal)
                    i_unit = get_unit_index(i_crystal)
                    self.energy_oPs_p[i_sample, i_unit*N_LYSOPERBLOCKX+i_x] += energy_A
                    self.energy_oPs_p[i_sample, N_MODULES*N_LYSOUNITS+i_unit*N_LYSOPERBLOCKX+i_x] += energy_B

        if integration_mode == None:
            # pPs data
            self.energy_pPs_p = self.energy_pPs
            # oPs data
            self.energy_oPs_p = self.energy_oPs
    
        self.integration_mode = integration_mode


    def apply_balancing(self, ratio=1.0):
        """
        Manually balance the dataset by removing samples from the larger class
        - param ratio: float, desired ratio of the number of pPs and oPs events
        """
        n_pPs = self.n_pPs
        n_oPs = self.n_oPs
        threshold_pPs = int(ratio*n_oPs)
        threshold_oPs = int(1/ratio*n_pPs)
        if n_pPs > threshold_pPs:
            self.energy_pPs = self.energy_pPs[:threshold_pPs]
        else:
            self.energy_oPs = self.energy_oPs[:threshold_oPs]
        

    def full_process(self, smearing_params={'smearing': 0.08, 'reference_energy': 662, 'type': 'FWHM'}, crystal_window=[0.01, 1_000], module_window=[0.01, 1_000], coincidence=True, integration_mode=None, ratio=1.0):
        """
        Apply full preprocessing to the data of pPs and oPs events
        - param smearing_params: dict, parameters for the energy smearing
        - param crystal_window: tuple, (E_min, E_max) energy window for the crystal cut
        - param energy_window: tuple, (E_min, E_max) energy window for the filter
        - param coincidence: bool, apply coincidence filter to the data
        - param integration_mode: str, 'module', 'unit', 'y', 'x' or None
        - param balanced: bool, balance the dataset if True
        - return: None
        """
        self.apply_smearing(**smearing_params)
        self.apply_crystal_cut(crystal_window)
        self.apply_module_cut(module_window, coincidence)
        self.apply_preprocessing(integration_mode)
        # self.apply_balancing(ratio)


    def get_data(self, preprocessed, balanced=False, shuffle=True):
        """
        Return data prepared appropriately for the machine learning models
        - param preprocessed: bool, return preprocessed data if True
        - param balanced: bool, balance the dataset if True
        - param shuffle: bool, shuffle the dataset if True
        - return: tuple, (X, y) where X is the feature matrix and y is the target vector
        """
        label_pPs = np.ones(self.n_pPs, dtype=int)
        label_oPs = np.zeros(self.n_oPs, dtype=int)
        if preprocessed:
            # label_pPs = np.zeros(self.n_pPs_p)
            label_pPs = np.ones(self.n_pPs, dtype=int)
            data_pPs = np.column_stack((self.energy_pPs_p, label_pPs))
            # label_oPs = np.ones(self.n_oPs_p)
            label_oPs = np.zeros(self.n_oPs, dtype=int)
            data_oPs = np.column_stack((self.energy_oPs_p, label_oPs))
        else:
            label_pPs = np.ones(self.n_pPs, dtype=int)
            data_pPs = np.column_stack((self.energy_pPs, label_pPs))
            label_oPs = np.zeros(self.n_oPs, dtype=int)
            data_oPs = np.column_stack((self.energy_oPs, label_oPs))
        data = np.concatenate((data_pPs, data_oPs), axis=0)
        if shuffle: np.random.shuffle(data)
        X, y = data[:, :-1], data[:, -1]
        y = y.astype(int)
        if balanced:
            X, y = balance_dataset(X, y)
        return X, y
        

if __name__ == '__main__':
    print('Debugging...')

    file_pPs = "/Users/simonwittum/Documents/Uni/Master/Semester2/SemesterProject/Analysis/data/txt_files/TwoGamma_1M.txt"
    file_oPs = "/Users/simonwittum/Documents/Uni/Master/Semester2/SemesterProject/Analysis/data/txt_files/ThreeGamma_1M.txt"

    # Testing new 'return_cut_efficiency' method
    An = SimulationAnalyzer(file_pPs, file_oPs)
    An.apply_smearing(0.05)
    print(An.n_pPs, An.n_oPs)
    An.apply_crystal_cut(energy_window=[480, 1_000])
    print(An.n_pPs, An.n_oPs)
    An.apply_module_cut(energy_window=[480, 1_000], coincidence=True)
    print(An.n_pPs, An.n_oPs)
    print(An.n_pPs/An.n_oPs)

